use std::collections::HashMap;
use std::convert::Infallible;
use std::net::{IpAddr, SocketAddr};

use lazy_static::lazy_static;

use clap::Parser;
use hyper::service::{make_service_fn, service_fn};
use hyper::{header, Body, Method, Request, Response, Server};
use walkdir::WalkDir;

mod route;

lazy_static! {
    static ref ARGS: Args = Args::parse();
    static ref ROUTES: HashMap<(Method, String), route::Response> = {
        WalkDir::new(ARGS.routes.clone())
            .into_iter()
            .filter_map(|e| e.ok())
            .filter(|e| e.file_type().is_file())
            .map(|e| e.path().to_owned())
            .map(|e| {
                let method = match e
                    .file_stem()
                    .expect("invalid names were filtered out")
                    .to_str()
                    .expect("valid UTF-8")
                    .to_lowercase()
                    .as_str()
                {
                    "+get" => Method::GET,
                    "+post" => Method::POST,
                    "+put" => Method::PUT,
                    "+patch" => Method::PATCH,
                    "+delete" => Method::DELETE,
                    method => panic!("unspported method: {}", method),
                };

                let mut route = e
                    .parent()
                    // TODO:  Handle this as an actual error
                    .expect("valid filestructure")
                    .to_str()
                    .expect("valid UTF-8")
                    // We just want the paths starting at the "routes" folder, so we
                    // remove /path/to/config/routes
                    .strip_prefix(&format!("{}/routes", ARGS.routes))
                    .unwrap()
                    .to_owned();

                // Routes that should evaluate to "/" will instead evaluate to "" since the
                // parent() function will evaluate to /path/to/config/routes and we then strip that
                // exact string from it.  We must add the trailing slash back in
                if route.is_empty() {
                    route = String::from("/")
                }

                let response = route::Response::from_file_path(&e);

                ((method, route), response)
            })
        .collect()
    };
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value_t = String::from("/etc/smeargle"))]
    routes: String,

    #[arg(short = 'H', long, default_value_t = String::from("127.0.0.1"))]
    host: String,

    #[arg(short, long, default_value_t = 8080)]
    port: u16,
}

async fn handle(req: Request<Body>) -> Result<Response<Body>, Infallible> {
    let path = req.uri().path().to_string();
    let method = req.method();

    let res = match ROUTES.get(&(method.to_owned(), path)) {
        Some(res) => Response::builder()
            .header(header::CONTENT_TYPE, res.content_type())
            .body(Body::from(res.body.clone()))
            .unwrap(),
        None => Response::builder()
            .status(404)
            .body(Body::from("Page Not Found"))
            .unwrap(),
    };

    Ok(res)
}

#[tokio::main]
async fn main() {
    println!("Loaded routes:");
    for (method, route) in ROUTES.keys() {
        println!("\t{method} - {route}");
    }

    // Construct our SocketAddr to listen on...
    let addr = SocketAddr::from((ARGS.host.parse::<IpAddr>().unwrap(), ARGS.port));

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(handle)) });

    // Then bind and serve...
    let server = Server::bind(&addr).serve(make_service);

    // And run forever...
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}
