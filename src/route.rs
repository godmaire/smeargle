use std::{fs, path::Path};

#[derive(Debug)]
pub enum ContentType {
    Json,
    Text,
    Html,
    Css,
}

#[derive(Debug)]
pub struct Response {
    pub(crate) content_type: ContentType,
    pub(crate) body: String,
}

impl Response {
    pub fn from_file_path(path: &Path) -> Self {
        let content_type = match path.extension().map(|e| e.to_str().unwrap_or("txt")) {
            Some("json") => ContentType::Json,
            Some("html") | Some("htm") => ContentType::Html,
            Some("css") => ContentType::Css,
            Some(_) => ContentType::Text,
            None => ContentType::Text,
        };

        let body = fs::read_to_string(path).unwrap_or("error: invalid body".to_string());

        Self { content_type, body }
    }

    pub fn content_type(&self) -> &str {
        match self.content_type {
            ContentType::Json => "application/json",
            ContentType::Text => "text/plain",
            ContentType::Html => "text/html",
            ContentType::Css => "text/css",
        }
    }
}
